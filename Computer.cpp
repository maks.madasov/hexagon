#include "Game.h"

using namespace sf;

int count = 0;

void Game::addMoveCoputer()
{
	minimax(mapList, 0, -INT_MAX, INT_MAX);

	std::cout << "\n====================\n";
}

void Game::findNewMove(std::vector<CircleShape>& tmpMap, std::vector<int>& maybeMove, int i)
{
	std::vector<CircleShape>::iterator ii = tmpMap.begin() + i - 1;
	lookingAround(ii, i, maybeMove);

	for (int j = 0; j < maybeMove.size(); ++j) {
		int a = maybeMove[j] < -1 ? maybeMove[j] + 100 - 1 : maybeMove[j] - 1;
		if (tmpMap[a].getFillColor() != def) {
			maybeMove.erase(maybeMove.begin() + j);
			--j;
		}
	}
}

void Game::newMapNewMove(std::vector<sf::CircleShape>& tmpMap, int from, int where)
{
	int a = where < -1 ? where + 100 - 1 : where - 1;
	int b = from - 1;
	sf::Color color = tmpMap[b].getFillColor();
	if (where < 0) {
		tmpMap[a].setFillColor(color);
		tmpMap[b].setFillColor(def);
	}
	else {
		tmpMap[a].setFillColor(color);
	}

	std::vector<CircleShape>::iterator ii = tmpMap.begin() + a;
	coloredAround(ii, a + 1, color, tmpMap);
}

int Game::minimax(std::vector<CircleShape>& Map, int deep, int alpha, int beta)
{
	std::vector<CircleShape> tmpMap = Map;
	Color color;
	std::map<std::pair<int, int>, int> moveRating;
	int sp1 = 0;
	int sp2 = 0;
	
	if (deep % 2 == 0)
		color = p1;
	else
		color = p2;


	std::vector<int> tmpMove;
	int i = 1;
	for (auto it = tmpMap.begin(); it != tmpMap.end(); ++it) {
		if ((*it).getFillColor() == color) {
			if (checkNewMove(it, i, tmpMap))
				tmpMove.push_back(i);
		}
		if ((*it).getFillColor() == p1)
			++sp1;
		if ((*it).getFillColor() == p2)
			++sp2;

		++i;
	}

	if (tmpMove.empty() && deep % 2 == 0)
		return -100;
	else if (tmpMove.empty() && deep % 2 != 0)
		return 100;

	
	for (auto it = tmpMove.begin(); it != tmpMove.end(); ++it) {

		std::vector<int> maybeMove;
		findNewMove(tmpMap, maybeMove, *it);

		for (auto it2 = maybeMove.begin(); it2 != maybeMove.end(); ++it2) {
			if (deep < 2) {
				auto tmp = tmpMap;
				newMapNewMove(tmpMap, *it, *it2);
				//printConsole(tmpMap);
				int res = minimax(tmpMap, deep + 1, alpha, beta);
				moveRating[{(*it), (*it2)}] = res;
				tmpMap = tmp;

				if (deep % 2 == 0)
					alpha = alpha > res ? alpha : res;
				else
					beta = beta < res ? beta : res;
				if (beta < alpha)
					break;
			}
		}

		maybeMove.clear();
	}
	tmpMove.clear();

	if (deep == 0)
	{
		int raiting = moveRating.begin()->second;
		std::vector<std::pair<int, int>> tmpMaybeMove;

		for (auto it = moveRating.begin(); it != moveRating.end(); ++it) {
			if (raiting == (*it).second)
				tmpMaybeMove.push_back((*it).first);
			else if (raiting < (*it).second) {
				tmpMaybeMove.clear();
				tmpMaybeMove.push_back((*it).first);
				raiting = (*it).second;
			}
		}

		int r = rand() % tmpMaybeMove.size();
		std::vector<std::pair<int, int>>::iterator rr = tmpMaybeMove.begin() + r;

		//std::cout << "\n" << (*rr).first << "   " << (*rr).second << "\n";

		//int where = moveRating.begin()->first.second;
		//int from = moveRating.begin()->first.first;

		int where = (*rr).second;
		int from = (*rr).first;


		newMapNewMove(mapList, from, where);

		toogleMove();

		for (auto it = moveRating.begin(); it != moveRating.end(); ++it)
		{
			int s = it->first.second < -1 ? it->first.second + 100 : it->first.second;
			std::cout << "\n<{" << it->first.first << ", " << s << "}, " << it->second << ">\n";
		}
		std::cout << "Count = " << count;
		count = 0;
	}
	
	return ratingNode(moveRating, deep, sp1, sp2);
}


int Game::ratingNode(std::map<std::pair<int, int>, int> ratingMove, int deep, int sp1, int sp2)
{
	int rating = sp1 - sp2;

	for (auto it = ratingMove.begin(); it != ratingMove.end(); ++it)
	{
		if (it == ratingMove.begin()) {
			rating = ratingMove.begin()->second;
		}
		if (deep % 2 == 0 && it->second > rating) {
			rating = it->second;
		}
		if (deep % 2 != 0 && it->second < rating) {
			rating = it->second;
		}
	}
	count++;
	return rating;
}

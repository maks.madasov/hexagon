#include "Game.h"

using namespace sf;

Game::Game()
{
	ContextSettings settings;
	settings.antialiasingLevel = 8;
	window.create(VideoMode(700, 600), "Hexagon", Style::Titlebar | Style::Close, settings);
	font.loadFromFile("2.ttf");
	circle.setRadius(30);
	circle.setPointCount(6);
	savePress = 0;
	def = Color(75, 75, 75);
	p1 = Color(230, 45, 59);
	p2 = Color(0, 129, 246);
	move = p2;
	scorePlayer1 = 3;
	scorePlayer2 = 3;
	X = 0;
	Y = 0;
	computer = false;
	srand(time(0));
	initMapList();
	initButton();
}

Game::~Game()
{
	mapList.clear();
	aroundColor.clear();
}

void Game::initButton()
{
	buttonComputerEasy.setSize(Vector2f(40, 40));
	buttonComputerEasy.setFillColor(Color(25, 25, 25));
	buttonComputerEasy.setPosition(Vector2f(530, 100));
	buttonComputerEasy.setOutlineColor(Color(0, 199, 254, 170));

	buttonComputerMedium = buttonComputerEasy;
	buttonComputerMedium.setPosition(Vector2f(580, 100));

	buttonComputerHard = buttonComputerEasy;
	buttonComputerHard.setPosition(Vector2f(630, 100));

	buttonNewGame.setSize(Vector2f(150, 80));
	buttonNewGame.setFillColor(Color(25, 25, 25));
	buttonNewGame.setPosition(Vector2f(530, 170));
	buttonNewGame.setOutlineColor(Color(0, 199, 254, 170));
	buttonNewGame.setOutlineThickness(-5);

	buttonEasyMedium = buttonNewGame;
	buttonEasyMedium.setSize(Vector2f(150, 40));
	buttonEasyMedium.setPosition(Vector2f(530, 270));

	buttonEasyHard = buttonEasyMedium;
	buttonEasyHard.setPosition(Vector2f(530, 330));

	buttonMediumHard = buttonEasyMedium;
	buttonMediumHard.setPosition(Vector2f(530, 380));
}

void Game::initMapList()
{
	tMapList.push_back(std::vector<int> { -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -1, -1, -1, -1,  1, -1, -1, -1, -1, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -1, -1, -0, -0, -0, -0, -0, -1, -1, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2,  2, -0, -0, -0, -0, -0, -0, -0,  2, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -0, -0, -0, -0, -1, -0, -0, -0, -0, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -0, -0, -0, -0, -0, -0, -0, -0, -0, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -0, -0, -0, -1, -0, -1, -0, -0, -0, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2,  1, -0, -0, -0, -0, -0, -0, -0,  1, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -1, -0, -0, -0, -0, -0, -0, -0, -1, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -1, -1, -1, -0,  2, 0, -1, -1, -1, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 });
	tMapList.push_back(std::vector<int> { -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 });

	for (auto it : tMapList) {
		for (auto it2 : it) {
			if (it2 > -1 ) std::cout << " ";
			std::cout << it2 << " ";
		}
		std::cout << " \n";
	}


	int mcy = -70, mcx = -20;
	for (int x = 0; x < 13; ++x) {
		for (int y = 0; y < 13; ++y) {
			if (tMapList[y][x] > -1) {
				CircleShape tmp(30, 6);
				tmp.setFillColor(def);
				tmp.rotate(90);
				if (tMapList[y][x] == 1)
					tmp.setFillColor(p1);
				if (tMapList[y][x] == 2)
					tmp.setFillColor(p2);
				tmp.setPosition(Vector2f((float)mcx + (float)x * 50, (float)mcy + (float)y * 55));
				mapList.push_back(tmp);
			}
		}
		if (x % 2 == 0) {
			mcy -= 27;
		}
		else {
			mcy += 27;
		}
	}

}

void Game::draw()
{
	sf::Text text("", font, 52);
	sf::Text num("", font, 30);

	window.clear(Color(30, 30, 30));

	for (auto hex = mapList.begin(); hex != mapList.end(); ++hex) {
		window.draw(*hex);	
	}

	/*int i = 1;
	for (auto hex = mapList.begin(); hex != mapList.end(); ++hex) {
		num.setFillColor(Color::Black);
		num.setString(std::to_string(i));
		num.setPosition(Vector2f((*hex).getPosition().x - 45, (*hex).getPosition().y));
		window.draw(num);
		++i;
	}*/

	text.setString(std::to_string(scorePlayer1));
	text.setFillColor(p1);
	text.setPosition(Vector2f(10, 10));
	window.draw(text);

	text.setString(std::to_string(scorePlayer2));
	text.setFillColor(p2);
	text.setPosition(Vector2f(475, 10));
	window.draw(text);

	
	sf::CircleShape t(10);
	if (move == p1) {
		t.setFillColor(p1);
	}
	else {
		t.setFillColor(p2);
	}
	t.setPosition(Vector2f(240, 10));
	window.draw(t);

	drawButton();

	window.display();
}

void Game::drawButton()
{
	Text textB("", font, 25);
	window.draw(buttonNewGame);
	window.draw(buttonComputerEasy);
	window.draw(buttonComputerMedium);
	window.draw(buttonComputerHard);
	window.draw(buttonEasyMedium);
	window.draw(buttonEasyHard);
	window.draw(buttonMediumHard);

	textB.setString("New Game");
	textB.setFillColor(Color(0, 199, 254));
	textB.setPosition(Vector2f(558, 30));
	window.draw(textB);
	textB.setPosition(Vector2f(558, 180));
	window.draw(textB);
	

	textB.setString("Vs. Computer");
	textB.setFillColor(Color(0, 199, 254));
	textB.setPosition(Vector2f(535, 60));
	window.draw(textB);
	textB.setString("Vs. Player");
	textB.setPosition(Vector2f(557, 210));
	window.draw(textB);

	textB.setString("1");
	textB.setPosition(Vector2f(544, 104));
	window.draw(textB);

	textB.setString("2");
	textB.setPosition(Vector2f(594, 104));
	window.draw(textB);

	textB.setString("3");
	textB.setPosition(Vector2f(644, 104));
	window.draw(textB);
}

void Game::test()
{

	while (window.isOpen())
	{
		input();

		draw();
	}
}

void Game::startGame()
{

	while (window.isOpen())
	{
		input();

		draw();
		
		
		if ((scorePlayer1 + scorePlayer2) == 58 || !checkAllNewMove()) {
			draw();
			printWin();
			sleep(seconds(3));
			newGame();
		}

		if (computer && move == p1) {
			addMoveCoputer();
		}

		score(mapList, scorePlayer1, scorePlayer2);
	}

}

void Game::input()
{
	Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();

		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Escape)
				window.close();

			if (event.key.code == sf::Keyboard::C)
				addMoveCoputer();
		}

		if (event.type == Event::MouseButtonPressed)
		{
			if (event.key.code == sf::Mouse::Left)
				if (Mouse::getPosition(window).x < 540) 
					pressMove();
				else
					pressButton();

			if (event.key.code == sf::Mouse::Right)
				pressDebug();
		}
	}
}


void Game::pressButton()
{
	Vector2i positionPixel = Mouse::getPosition(window);
	Vector2f position = window.mapPixelToCoords(positionPixel);
	if (buttonComputerEasy.getGlobalBounds().contains(position)) {
		buttonComputerEasy.setOutlineThickness(-5);
		buttonNewGame.setOutlineThickness(0);
		buttonComputerMedium.setOutlineThickness(0);
		buttonComputerHard.setOutlineThickness(0);
		newGame();
		computer = true;
	}
	if (buttonComputerMedium.getGlobalBounds().contains(position)) {
		buttonComputerMedium.setOutlineThickness(-5);
		buttonComputerEasy.setOutlineThickness(0);
		buttonComputerHard.setOutlineThickness(0);
		buttonNewGame.setOutlineThickness(0);
		newGame();
		computer = true;
	}
	if (buttonComputerHard.getGlobalBounds().contains(position)) {
		buttonComputerHard.setOutlineThickness(-5);
		buttonComputerEasy.setOutlineThickness(0);
		buttonComputerMedium.setOutlineThickness(0);
		buttonNewGame.setOutlineThickness(0);
		newGame();
		computer = true;
	}
	if (buttonNewGame.getGlobalBounds().contains(position)) {
		buttonNewGame.setOutlineThickness(-5);
		buttonComputerEasy.setOutlineThickness(0);
		buttonComputerMedium.setOutlineThickness(0);
		buttonComputerHard.setOutlineThickness(0);
		newGame();
		computer = false;
	}
}

void Game::pressDebug()
{
	Vector2i positionPixel = Mouse::getPosition(window);
	Vector2f position = window.mapPixelToCoords(positionPixel);
	for (auto hex = mapList.begin(); hex != mapList.end(); ++hex)
	{
		if ((*hex).getGlobalBounds().contains(position.x, position.y) && (*hex).getFillColor() == move) {
			(*hex).setFillColor(def);
		}
		else if ((*hex).getGlobalBounds().contains(position.x, position.y) && (*hex).getFillColor() == def) {
			(*hex).setFillColor(move);
		}
	}
}

void Game::pressMove()
{
	Vector2i positionPixel = Mouse::getPosition(window);
	Vector2f position = window.mapPixelToCoords(positionPixel);

	int i = 1;
	for (auto hex = mapList.begin(); hex != mapList.end(); ++hex)
	{
		if ((*hex).getGlobalBounds().contains(position.x, position.y) && (*hex).getFillColor() == move) {
			if ((*hex).getOutlineThickness() == -5) {
				allClearOutline();
			}
			else {
				savePress = i - 1;
				allClearOutline();
				(*hex).setOutlineThickness(-5);
				(*hex).setOutlineColor(Color::Yellow);

				addMove(hex, i);
			}
			break;
		}

		if ((*hex).getGlobalBounds().contains(position.x, position.y) &&
			(*hex).getOutlineColor() == Color::Yellow && (*hex).getOutlineThickness() == -5) {
			allClearOutline();
			(*hex).setFillColor(move);
			coloredAround(hex, i, move, mapList);
			toogleMove();
			break;
		}

		if ((*hex).getGlobalBounds().contains(position.x, position.y) &&
			(*hex).getOutlineColor() == Color::Green && (*hex).getOutlineThickness() == -5) {
			allClearOutline();
			(*hex).setFillColor(move);
			clearSavePress();
			coloredAround(hex, i, move, mapList);
			toogleMove();
			break;
		}
		i++;
	}

	score(mapList, scorePlayer1, scorePlayer2);
}

void Game::allClearOutline()
{
	for (auto hex = mapList.begin(); hex != mapList.end(); ++hex)
		(*hex).setOutlineThickness(0);
}

void Game::toogleMove()
{
	if (move == p1)
		move = p2;
	else
		move = p1;
}

void Game::clearSavePress()
{
	mapList[savePress].setFillColor(def);
}

bool Game::checkAllNewMove()
{
	int i = 1;
	for (auto it = mapList.begin(); it != mapList.end(); ++it) {
		if (checkNewMove(it, i, mapList) && (*it).getFillColor() == move) {
			return true;
		}
		++i;
	}
	return false;
}

bool Game::checkNewMove(std::vector<CircleShape>::iterator& hex, int i, std::vector<CircleShape> &map)
{
	lookingAround(hex, i, aroundColor);

	for (int j = 0; j < aroundColor.size(); ++j) {
		int a = aroundColor[j] < -1 ? aroundColor[j] + 100 - 1: aroundColor[j] - 1;
		if (map[a].getFillColor() != def) {
			aroundColor.erase(aroundColor.begin() + j);
			--j;
		}
	}

	if (aroundColor.size() == 0) {
		return false;
	}
	aroundColor.clear();
	return true;
}

void Game::noNewMove()
{
	for (auto it = mapList.begin(); it != mapList.end(); ++it) {
		if ((*it).getFillColor() == def) {
			if (move == p1)
				(*it).setFillColor(p2);
			else
				(*it).setFillColor(p1);
		}
	}
}

void Game::coloredAround(std::vector<CircleShape>::iterator& hex, int i, sf::Color c, std::vector<sf::CircleShape>& map)
{
	lookingAround(hex, i, aroundColor);

	for (auto it = aroundColor.begin(); it != aroundColor.end(); ++it) {
		int a = (*it) - 1;
		if (a < 0) continue;
		if (map[a].getFillColor() != c && map[a].getFillColor() != def) {
			map[a].setFillColor(c);
		}
	}

	aroundColor.clear();
}

void Game::addMove(std::vector<CircleShape>::iterator& hex, int i)
{
	lookingAround(hex, i, aroundColor);

	for (auto it = aroundColor.begin(); it != aroundColor.end(); ++it) {
		int a = (*it) - 1;
		if (*it > -1) {
			if (mapList[a].getFillColor() == def) {
				mapList[a].setOutlineThickness(-5);
				mapList[a].setOutlineColor(Color::Yellow);
			}
		}
		else {
			a += 100;
			if (mapList[a].getFillColor() == def) {
				mapList[a].setOutlineThickness(-5);
				mapList[a].setOutlineColor(Color::Green);
			}
		}
	}

	aroundColor.clear();
}

void Game::printConsole(std::vector<sf::CircleShape> &map)
{
	auto it = map.begin();
	std::cout << "\n ====== \n";
	for (int i = 0; i < 58; ++i) {
		if ((*it).getFillColor() == p1)
			std::cout << 1 << " ";
		else if ((*it).getFillColor() == p2)
			std::cout << 2 << " ";
		else
			std::cout << 0 << " ";

		if (i == 4 || i == 10 || i == 17 || i == 24 || i == 32 || i == 39 || i == 46 || i == 52 || i == 57)
			std::cout << "\n";

		++it;
	}
	std::cout << "\n ====== \n";
}

void Game::addAroundLooking(int index, std::vector<int> &vec)
{
	if (std::find(vec.begin(), vec.end(), index) == vec.end())
		vec.push_back(index);
}

void Game::getXY(std::vector<CircleShape>::iterator& hex, int i)
{
	int count = 0;
	for (int x = 0; x < 13; ++x) {
		for (int y = 0; y < 13; ++y) {
			if (tMapList[y][x] > -1)
				++count;
			if (count == i) {
				X = x;
				Y = y;
				return;
			}
		}
	}
}

int Game::getIndex(int x, int y)
{
	int count = 0;
	for (int x2 = 0; x2 < 13; ++x2) {
		for (int y1 = 0; y1 < 13; ++y1) {
			if (tMapList[y1][x2] > -1)
				++count;
			if (x2 == x && y1 == y) {
				return count;
			}
		}
	}
}

void Game::lookingAround(std::vector<CircleShape>::iterator& hex, int i, std::vector<int>& vec)
{
	getXY(hex, i);
	int x = X;
	int y = Y;

	x = X; y = Y - 1;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y), vec);
	x = X; y = Y + 1;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y), vec);
	x = X; y = Y - 2;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X; y = Y + 2;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);

	int a = 0;
	if (X % 2 == 1)
		a = 1;

	x = X + 1; y = Y - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y), vec);
	x = X + 1; y = Y + 1 - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y), vec);

	x = X - 1; y = Y - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y), vec);
	x = X - 1; y = Y + 1 - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y), vec);

	x = X + 2; y = Y;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X + 2; y = Y + 1;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X + 2; y = Y - 1;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);

	x = X - 2; y = Y;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X - 2; y = Y + 1;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X - 2; y = Y - 1;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);

	x = X + 1; y = Y - 1 - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(X + 1, Y - 1 - a) - 100, vec);
	x = X - 1; y = Y - 1 - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X + 1; y = Y + 2 - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	x = X - 1; y = Y + 2 - a;
	if (tMapList[y][x] > -1)
		addAroundLooking(getIndex(x, y) - 100, vec);
	
}

void Game::printWin()
{
	sf::Text text("", font, 52);
	if (scorePlayer1 > scorePlayer2 && !computer) {
		text.setString("Win Player Red");
		text.setFillColor(p1);
	}
	else if (scorePlayer1 < scorePlayer2 && !computer) {
		text.setString("Win Player Blue");
		text.setFillColor(p2);
	}
	else if (scorePlayer1 > scorePlayer2 && computer) {
		text.setString("Win Computer");
		text.setFillColor(p1);
	}
	else if (scorePlayer1 < scorePlayer2 && computer) {
		text.setString("Win Player");
		text.setFillColor(p2);
	}
	else {
		text.setString("Drawn game");
		text.setFillColor(Color(147, 39, 236));
	}

	text.setPosition(150, 530);
	window.draw(text);
	window.display();
}

void Game::newGame()
{
	int a = 1;
	move = p2;
	for (auto it = mapList.begin(); it != mapList.end(); ++it)
	{
		(*it).setOutlineThickness(0);
		(*it).setFillColor(Color(75,75,75));
		if (a == 1 || a == 33 || a == 54)
			(*it).setFillColor(p2);
		if (a == 5 || a == 26 || a == 58)
			(*it).setFillColor(p1);
		++a;
	}
	score(mapList, scorePlayer1, scorePlayer2);
}

void Game::score(std::vector<sf::CircleShape>& Map, int& sp1, int& sp2)
{
	sp1 = 0;
	sp2 = 0;
	for (auto it = Map.begin(); it != Map.end(); ++it)
	{
		if ((*it).getFillColor() == p1)
			sp1++;
		if ((*it).getFillColor() == p2)
			sp2++;
	}
}


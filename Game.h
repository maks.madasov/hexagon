#pragma once
#include <SFML/Graphics.hpp>
#include <map>
#include <ctime>
#include <iostream>
#include <vector>
#include <algorithm>

class Game
{
private:
	sf::RenderWindow window;
	sf::CircleShape circle;
	std::vector<sf::CircleShape> mapList;
	std::vector<int> aroundColor;
	sf::Color move;
	sf::Font font;
	sf::Color def;
	sf::Color p1;
	sf::Color p2;
	sf::RectangleShape buttonComputerEasy;
	sf::RectangleShape buttonComputerMedium;
	sf::RectangleShape buttonComputerHard;
	sf::RectangleShape buttonNewGame;

	sf::RectangleShape buttonEasyMedium;
	sf::RectangleShape buttonEasyHard;
	sf::RectangleShape buttonMediumHard;
	//
	std::vector<std::vector<int>> tMapList;
	int X;
	int Y;
	//
	int savePress;

	bool computer;

	int scorePlayer1;
	int scorePlayer2;

	void initMapList();
	void initButton();

	void input();

	void printWin();

	void pressDebug();
	void pressMove();
	void pressButton();

	void allClearOutline();

	void toogleMove();

	void clearSavePress();

	void coloredAround(std::vector<sf::CircleShape>::iterator& hex, int i, sf::Color c, std::vector<sf::CircleShape>& map);
	bool checkNewMove(std::vector<sf::CircleShape>::iterator& hex, int i, std::vector<sf::CircleShape>& map);
	bool checkAllNewMove();

	void noNewMove();

	void addMove(std::vector<sf::CircleShape>::iterator& hex, int i);

	void lookingAround(std::vector<sf::CircleShape>::iterator& hex, int i, std::vector<int>& vec);

	void newGame();

	void draw();
	void drawButton();

	void score(std::vector<sf::CircleShape>& Map, int& sp1, int& sp2);

	void getXY(std::vector<sf::CircleShape>::iterator& hex, int i);
	int getIndex(int x, int y);

	void addAroundLooking(int index, std::vector<int>& vec);
	void addMoveCoputer();

	int minimax(std::vector<sf::CircleShape>& Map, int deep, int alpha, int beta);

	int ratingNode(std::map<std::pair<int, int>, int> rating, int deep, int sp1, int sp2);

	void printConsole(std::vector<sf::CircleShape>& map);

	void findNewMove(std::vector<sf::CircleShape>& map, std::vector<int>& maybeMove, int i);

	void newMapNewMove(std::vector<sf::CircleShape>& map, int from, int where);

public:
	Game();
	void startGame();
	void test();
	~Game();
};
